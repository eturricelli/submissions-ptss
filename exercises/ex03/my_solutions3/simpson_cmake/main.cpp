#include <iostream>
#include <cmath>
#include "integration.hpp"
#define _USE_MATH_DEFINES

double my_sin(double x) {
    return sin(x);
}

int main() {

    std::cout << "Integral of sin(x) in [0,pi] is " << integration(0.0, M_PI, 100, my_sin) << "\n";
    int max_bins = 10;
    std::cout << "Different bins\n";
    for ( int i = 1; i < max_bins; i++) {
        std::cout << i << "   " << integration(0.0, M_PI, i, my_sin) << "\n";
    }

    return 0;
}