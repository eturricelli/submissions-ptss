

double integration(double a, double b, int N, double (*my_func)(double x));

// Pre-conditions
/*
- N must be a positive integer
- b >= a

Post-conditions
- The result is the integral of the specified function in the interval [a,b]

*/