#include <iostream>
#include <cmath>
#include <cassert>
#define _USE_MATH_DEFINES

double integration(double a, double b, int N, double (*my_func)(double x)) {
    assert(N>0);
    assert(b > a);
    double delta = (b-a)/N;
    double result = my_func(a);

    for (int i = 1; i< N; i++) {
        if (i%2==1) {
            result += 4*my_func(a+i*delta);
        }
        else {
            result += 2*my_func(a+i*delta);
        }
    }
    result += my_func(b);
    return result*delta/3.0;
}

