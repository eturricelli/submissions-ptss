#include <iostream> 
#include <limits>
#include "epsilon.hpp"


template <typename T>
T machine_epsilon() {
    // T epsilon = epsilon_arg;
    T epsilon = 1;
    while(1.0 + 0.5*epsilon != 1.0) {
        epsilon /= 2;
    }

    return epsilon;    
}


int main() {

    //float
    std::cout << "FLOAT" <<"\n";
    std::cout << "The machine epsilon is " << machine_epsilon<float>() << "\n";
    std::cout << "The exact numer limit for float is " << std::numeric_limits<float>::epsilon() << "\n\n";

    //double
    std::cout << "DOUBLE" <<"\n";
    std::cout << "The machine epsilon is " << machine_epsilon<double>() << "\n";
    std::cout << "The exact numer limit for double is " << std::numeric_limits<double>::epsilon() << "\n\n";

    //long double
    std::cout << "LONG DOUBLE" <<"\n";
    std::cout << "The machine epsilon is " << machine_epsilon<long double>() << "\n";
    std::cout << "The exact numer limit for long double is " << std::numeric_limits<long double>::epsilon() << "\n";
    return 0;
}