#include <iostream>

template <typename T>
T machine_epsilon() {
    // T epsilon = epsilon_arg;
    T epsilon = 1;
    while(1.0 + 0.5*epsilon != 1.0) {
        epsilon /= 2;
    }

    return epsilon;    
}
