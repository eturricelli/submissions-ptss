// This function computes the machine epsilon using different types and make a comparison with the inner implementation of C++

// Reference: https://en.wikipedia.org/wiki/Machine_epsilon
// The following simple algorithm can be used to approximate the machine epsilon,
// to within a factor of two (one order of magnitude) of its true value, using a linear search.
// TEMPLATE programming

template <typename T>
T machine_epsilon();
