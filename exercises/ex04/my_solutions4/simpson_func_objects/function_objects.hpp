#include <iostream>
#include <cmath>
#include <cassert>
#define _USE_MATH_DEFINES

// Pre-conditions
/*
- N must be a positive integer
- b >= a
- a and b are templated, so I put the implementation of the function in this header
- we do not need cpp file, the compiler needs to know in the declaration what type to use

Post-conditions
- The result is the integral of the specified function in the interval [a,b]

*/


class Func_Obj {
    private:
        double lambda;

    public:
        Func_Obj(double lambda_value) : lambda(lambda_value) { }

        // This operator overloading enables calling 
        // operator function () on objects of increment
        double operator () (double x) const { 
            return exp(-lambda*x); 
        } 
        
        // you need friend! Otherwise the class std::ostream can't access Func_obj members
        friend std::ostream &operator<<( std::ostream  &out, Func_Obj &F_O) {
            out << "( Lambda: " << F_O.lambda << " )\n";
            return out;
        }
};

template <typename T>
double integration(const T a, const T b, const int N, Func_Obj my_obj) {
    assert(N>0);
    assert(b > a);
    double delta = (b-a)/N;
    double result = my_obj(a);

    for (int i = 1; i< N; i++) {
        if (i%2==1) {
            result += 4*my_obj(a+i*delta);
        }
        else {
            result += 2*my_obj(a+i*delta);
        }
    }
    result += my_obj(b);
    return result*delta/3.0;
}


// template <typename T>
/*
double integration(const double a, const double b, const int N, Func_Obj my_obj(double lambda_value)) {
    assert(N>0);
    assert(b > a);
    double delta = (b-a)/N;
    double result = my_obj(a);

    for (int i = 1; i< N; i++) {
        if (i%2==1) {
            result += 4*my_obj(a+i*delta);
        }
        else {
            result += 2*my_obj(a+i*delta);
        }
    }
    result += my_obj(b);
    return result*delta/3.0;
}
*/


