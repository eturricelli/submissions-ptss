#include <iostream>
#include <cmath>
#include "function_objects.hpp"
#define _USE_MATH_DEFINES


int main() {
    double lambda = 1;
    Func_Obj object(lambda);
    std::cout << object << "\n";
    std::cout << "The integral in [0,1] of exp(-lambda*x) is " << integration<double>(0.0, 1.0, 128, object) << "\n";
    return 0;
}