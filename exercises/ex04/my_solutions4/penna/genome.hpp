#include <vector>

class Genome {

    public:
        Genome() {}; // generation of a random sequence (used for the first generation)
        Genome(bool child) {}; // creation of a child starting from a mother
        void mutation(int position); // change a single bit in a particual position of a sequence
        ~Genome() {}; // destructor when an animal dies
    

    private:
        int B; // span of time considered in the simulation
        std::vector<int> sequence; // vector containing the genes
};