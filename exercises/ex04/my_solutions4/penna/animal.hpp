#include "genome.hpp"

class Animal {

    public:
        Animal() {}; // constructor that initializes the genome sequence
        static int counter; // count the animals alive ~ population 
        ~Animal() {}; // destructor when an animal dies
        void give_birth(); // have a child (only one is allowed)
        void prob_life(); // after the threshold, an individual is alive only with a certain probability
    private:
        Genome sequence();
        int mutation_rate; // mutation rate that determines how many bits are gonna change for the child
        int reproduction_age; // minimum age to have a child
        int life_threshold; // threshold for mutations allowed before death risks
        int max_population; // maximum numbers of individuals in the population
    
};