#include <iostream>
#include <math.h>
#define _USE_MATH_DEFINES
// Reference: https://en.wikipedia.org/wiki/Simpson%27s_rule
// Simpson integration

double integration(double a, double b, double N) {
    double delta = (b-a)/N;
    double result = sin(a);

    for (int i = 1; i< N; i++) {
        if (i%2==1) {
            result += 4*sin(a+i*delta);
        }
        else {
            result += 2*sin(a+i*delta);
        }
    }
    result += sin(b);
    return result*delta/3.0;
}

int main() {

    std::cout << "Simpson integration of sin(x) in [0, pi] is \n";
    std::cout << integration(0, M_PI, 100) << "\n";

    return 0;
}