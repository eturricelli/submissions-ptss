#include <iostream>

// Reference: https://en.wikipedia.org/wiki/Machine_epsilon
// The following simple algorithm can be used to approximate the machine epsilon,
// to within a factor of two (one order of magnitude) of its true value, using a linear search.

int main() {

    double epsilon = 1;
    while(1.0 + 0.5*epsilon != 1.0) {
        epsilon /= 2;
    }

    std::cout << "The machine epsilon is " << epsilon << "\n";

    return 0;
}