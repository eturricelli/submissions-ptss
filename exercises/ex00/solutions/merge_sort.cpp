/*
 * Programming Techniques for Scientific Simulations I
 * HS 2020
 * Exercise 0.3
 * 2020-09-23
 * Michal Sudwoj <msudwoj@student.ethz.ch>
 */

#include <cassert>   // for assert
#include <cmath>     // for std::sin
#include <algorithm> // for std::is_sorted
#include <iostream>  // for std::cin, std::cout
#include <memory>    // for std::unique_ptr
#include <vector>    // for std::vector

using element_t = double;
using array_t   = std::vector<element_t>;

void merge_sort(array_t & sequence) noexcept;
void merge_sort_detail(element_t sequence[], std::size_t n, element_t buffer[]) noexcept;

int main() {
	std::size_t n;
	std::cout << "Set length of the sequence to be sorted:\n";
	std::cin >> n;

	array_t sequence;
	// reserve space to avoid reallocations while growing
	sequence.reserve(n);
	for (int i = 0; i < n; ++i) {
		sequence.push_back(std::sin(i));
	}

	std::cout << "Original sequence:\n";
	for (int i = 0; i < n; ++i) {
		std::cout << sequence[i] << ' ';
	}
	std::cout << '\n';

	merge_sort(sequence);

	std::cout << "Sorted sequence:\n";
	for (int i = 0; i < n; ++i) {
		std::cout << sequence[i] << ' ';
	}
	std::cout << '\n';

	assert(std::is_sorted(sequence.begin(), sequence.end()));
}

// nice interface that lauches merge_sort_detail
void merge_sort(array_t & sequence) noexcept {
	std::size_t n = sequence.size();
	// use unique_ptr to convey ownership, and not leak memory
	std::unique_ptr<element_t[]> buffer(new element_t[n]);
	merge_sort_detail(sequence.data(), n, buffer.get());
}

// do the real work here
void merge_sort_detail(element_t sequence[], std::size_t n, element_t buffer[]) noexcept {
	if (n == 1) {
		return;
	}

	// divide
	element_t * left  = sequence;
	element_t * right = sequence + n / 2;

	// and conquer
	merge_sort_detail(left,  n / 2,     buffer);
	merge_sort_detail(right, n - n / 2, buffer + n / 2);

	// merge into buffer
	auto buffer_iterator = buffer;
	while (left < sequence + n / 2 && right < sequence + n) {
		if (*left < *right) {
			*buffer_iterator++ = *left++;
		} else {
			*buffer_iterator++ = *right++;
		}
	}
	// deal with left-overs
	while (left < sequence + n / 2) {
		*buffer_iterator++ = *left++;
	}
	while (right < sequence + n) {
		*buffer_iterator = *right++;
	}

	// copy buffer back into sequence
	while (buffer_iterator-- > buffer) {
		*(sequence + (buffer_iterator - buffer)) = *buffer_iterator;
	}

	assert(std::is_sorted(sequence, sequence + n));
}
