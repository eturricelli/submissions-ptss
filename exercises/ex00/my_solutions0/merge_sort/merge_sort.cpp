#include <iostream>
#include <deque>

void print_deque(std::deque<double> deq) {
    for(unsigned int i = 0; i < deq.size(); i++) {
        std::cout << deq[i] << "\t";
    }
    std::cout << "\n";
}

std::deque<double> merge(std::deque<double> deque1, std::deque<double> deque2) {
    std::deque<double> result;
    while(deque1.size()>0 && deque2.size()>0) {       
        if(deque1.front() < deque2.front()) {
            result.push_back(deque1.front());

            deque1.pop_front();
        }
        else {
            result.push_back(deque2.front());
            deque2.pop_front();

        }
    }
    
    while(deque1.size()>0) {
        result.push_back(deque1.front());
        deque1.pop_front();
    }
    while(deque2.size()>0) {
        result.push_back(deque2.front());
        deque2.pop_front();
    }  
    return result;
}


void merge_sort(std::deque<double> &container) {
    if(container.size() == 1) {
        return;
    }
    std::deque<double> left;
    std::deque<double> right;
    std::deque<double>::iterator middleItr(container.begin() + container.size()/2);

    for(auto it = container.begin(); it != container.end(); it++) {
        if(std::distance(it, middleItr) > 0) {
            left.push_back(*it);
        }
        else {
            right.push_back(*it);
        }
    }   
    merge_sort(left);
    merge_sort(right);

    container = merge(left, right);
}


int main() {
    int n;
    std::cout << "Select the number that you want to insert \n";
    std::cin >> n;
    std::cout << "Insert the numbers;" << "\n";
    std::deque<double> sequence;
    for (unsigned int i=0; i < n; i++) {
        double value;
        std::cin >> value;
        sequence.push_back(value);
    }

    std::cout << "Print the elements\n";
    for (unsigned int i=0; i<sequence.size(); i++) {
        std::cout << sequence.at(i) << "\t";
        std::cout << "\n";
    }

    /*
    Debugging
    std::deque<double> prova1;
    prova1.push_back(10);
    prova1.push_back(20);
    prova1.push_back(500);

    std::deque<double> prova2;
    prova2.push_back(30);
    prova2.push_back(40); 

    std::deque<double> prova3 = merge(prova1, prova2); 
    */   


    merge_sort(sequence);

    std::cout << "Print the elements sorted\n";
    for (unsigned int i=0; i<sequence.size(); i++) {
        std::cout << sequence.at(i) << "\t";
        std::cout << "\n";
    }

    return 0;
}
