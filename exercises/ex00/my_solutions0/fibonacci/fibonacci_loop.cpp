#include <iostream>

int fibonacci(int n) {
    if (n==0) {
        return 0;
    }
    if(n==1) {
        return 1;
    } 
    int fib1 = 0; // fibonacci(n-1)
    int fib2 = 1; // fibonacci(n-2)
    int result;
    for (unsigned int i = 0; i < n; i++) {
        result = fib1 + fib2;
        fib2 = fib1;
        fib1 = result;
    }
    return result;
}



int main() {

    int n = 5;
    for (int i=0; i <= n; i++) {
        std::cout << "Fibonacci(" << i << ") is " << fibonacci(i) << "\n";
    }    
    return 0;
}