#include <iostream>

// Reference: https://en.wikipedia.org/wiki/Euclidean_algorithm

int gcd(int a, int b) {
    while (a!=b) {
        if(a>b) {
            a -= b;
        }
        else {
            b -= a;
        }
    }
    return a;

}

int main() {

    int a = 24;
    int b = 30;

    std::cout << "gcd(" << a << ", " << b << ") is " << gcd(a, b) << "\n";

    return 0;
}