#include <iostream>

// Reference: https://en.wikipedia.org/wiki/Euclidean_algorithm

int gcd(int a, int b) {
    if (b==0) {
        return a;
    }
    else {
        return gcd(b, a % b);
    }
}

int main() {

    int a = 24;
    int b = 30;

    std::cout << "gcd(" << a << ", " << b << ") is " << gcd(a, b) << "\n";

    return 0;
}