#include <iostream>
#include <cmath>
#include "integration.hpp"
#define _USE_MATH_DEFINES

const double my_sin(double x) {
    return sin(x);
}

const double test_func(double x) {
    return x*(1-x);
}

int main() {

    std::cout << "Integral of sin(x) in [0,pi] is " << integration(0.0, M_PI, 100, my_sin) << "\n";
    int max_bins = 10;
    std::cout << "Different bins\n";
    for ( int i = 1; i < max_bins; i++) {
        std::cout << i << "   " << integration(0.0, M_PI, i, my_sin) << "\n";
    }

    std::cout << "Test simpson rule \n";
    std::cout << "Integral of x*(1-x) in [0, 1] is " << integration(0.0, 1.0, 55, test_func) << "\n";

    return 0;
}