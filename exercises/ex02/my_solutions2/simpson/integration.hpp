

double integration(const double a, const double b, const int N, const double (*my_func)(double x));

// Pre-conditions
/*
- N must be a positive integer
- b >= a

Post-conditions
- The result is the integral of the specified function in the interval [a,b]

*/