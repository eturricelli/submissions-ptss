#include <iostream>
#include <vector>

int main() {

    int n_max = 10;
    double static_array[n_max];

    // static approach

    int n;
    std::cout << "Select the number of elements you want to insert\n";
    std::cin >> n;

    std::cout << "Static approach\n";
    double sum = 0;
    for (unsigned int i=0; i<n; i++) {
        double value;
        std::cin >> value;
        static_array[i] = value;
        sum += value;
    }
    // normalization
    for (unsigned int i=0; i<n; i++) {
        static_array[i] /= sum;
    }

    // print in reverse order
    for (int i = n-1; i >= 0; i--) {
        std::cout << static_array[i] << "\t";
    }
    std::cout << "\n";


    // dynamic approach
    std::cout << "Dynamic approach\n";
    std::vector<double> vect;
    double result = 0;
    
    for(unsigned int i = 0; i < n; i++) {
        double value;
        std::cin >> value;
        vect.push_back(value);
        result += value;
    }
    
    // normalization
    for (unsigned int i = 0; i < n; i++) {
        vect[i] /= result;
    }

    // print in reverse order
    for (int i = n-1; i >= 0; i--) {
        std::cout << vect[i] << "\t";
    }
    std::cout << "\n";

    return 0;
}